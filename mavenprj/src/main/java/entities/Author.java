package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Author")
public class Author {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int uid;
	String name;  
	String pw_again;
	String email;  
	String password; 
	String position;
	public Author() {
		// TODO Auto-generated constructor stub
	super();
	}
	
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

	public int getUid() {
		return uid;
	}


	public void setUid(int uid) {
		this.uid = uid;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPw_again() {
		return pw_again;
	}


	public void setPw_again(String pw_again) {
		this.pw_again = pw_again;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}



}

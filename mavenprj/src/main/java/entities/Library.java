package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.mysql.cj.x.protobuf.MysqlxDatatypes.Array;

@Entity
public class Library {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int b_id;
	String b_name;
	String path;
	String usermail;
	
	
	
	public Library() {
		// TODO Auto-generated constructor stub
	}

	
	public Library(int b_id, String b_name,String usermail,String path) {
		this.b_id = b_id;
		this.usermail = usermail;
		this.b_name = b_name;
		
	}




	public String getB_name() {
		return b_name;
	}



	public void setB_name(String b_name) {
		this.b_name = b_name;
	}



	


	public int getB_id() {
		return b_id;
	}


	public void setB_id(int b_id) {
		this.b_id = b_id;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public String getUsermail() {
		return usermail;
	}


	public void setUsermail(String usermail) {
		this.usermail = usermail;
	}



	
	
	
	
	
}

package entities;
import java.util.List;

import javax.faces.bean.ManagedBean;  
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "User")
public class User{  

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
public int uid;
String name;  
String pw_again;
String email;  
String password;  
String position;


 public User() {
	// TODO Auto-generated constructor stub
}
 
public User(int uid) {
	super();
	this.uid = uid;
	
}

public String fileLocation() {
return "/PP.jpg";

}
//
//public void createEmployee() {
//	// TODO Auto-generated method stub
//	
//	EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
//    
//    EntityManager entitymanager = emfactory.createEntityManager( );
//    entitymanager.getTransaction( ).begin( );
//
//    Employee employee = new Employee( ); 
//    employee.setEid( 12011 );
//    employee.setEname( "Gopal" );
//    employee.setSalary( 40000 );
//    employee.setDeg( "Technical Manager" );
//    
//    entitymanager.persist( employee );
//    entitymanager.getTransaction( ).commit( );
//
//    entitymanager.close( );
//    emfactory.close( );
//
//}



public int getUid() {
	return uid;
}

public String getPosition() {
	return position;
}

public void setPosition(String position) {
	this.position = position;
}

public void setUid(int uid) {
	this.uid = uid;
}

public String getName() {  
return name;  
}  
public void setName(String name) {  
this.name = name;  
}  
public String getEmail() {  
return email;  
}  
  
public void setEmail(String email) {  
this.email = email;  
}  
public String getPassword() {  
return password;  
}  
public void setPassword(String password) {  
this.password = password;  
}  



public String getPw_again() {
	return pw_again;
}
public void setPw_again(String pw_again) {
	this.pw_again = pw_again;
}



}  
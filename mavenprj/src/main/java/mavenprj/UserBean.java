package mavenprj;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.*;  
import javax.persistence.criteria.*;  

import entities.Author;
import entities.User;

import java.io.Serializable;
 
@ManagedBean(name= "userbean")
@SessionScoped
public class UserBean implements Serializable {
 
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String userName = "";
	 String e_mail = "";
	 String password = "";
	 String pw_again = "";
	 String position = "";



	int a;
	 public String info = null;
	

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getE_mail() {
		return e_mail;
	}

	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPw_again() {
		return pw_again;
	}

	public void setPw_again(String pw_again) {
		this.pw_again = pw_again;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}


	
	
	public int createUser() {//this method creates both author and normal user
		
	    int size = 0;
	    
	    if(position.equals("User")) {
	    	EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
			EntityManager entitymanager = emfactory.createEntityManager( );
		    entitymanager.getTransaction().begin( );
		    
		    
	    CriteriaBuilder cb=entitymanager.getCriteriaBuilder();  
        AbstractQuery<User> cq1=cb.createQuery(User.class);  
        Root<User> user1=cq1.from(User.class);  

        cq1.where(cb.equal(user1.get("email"),e_mail ));
		
        CriteriaQuery<User> select1 = ((CriteriaQuery<User>) cq1).select(user1);  
        TypedQuery<User> tq1 = entitymanager.createQuery(select1);  
        java.util.List<User> list1 = tq1.getResultList();  
        
        System.out.println("sayzı" + list1.size());
        size = list1.size();
	    }
	    
	    else {
	    	EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
			EntityManager entitymanager = emfactory.createEntityManager( );
		    entitymanager.getTransaction().begin( );

	    	CriteriaBuilder cb=entitymanager.getCriteriaBuilder();  
	        AbstractQuery<Author> cq1=cb.createQuery(Author.class);  
	        Root<Author> user1=cq1.from(Author.class);  

	        cq1.where(cb.equal(user1.get("email"),e_mail ));
			
	        CriteriaQuery<Author> select1 = ((CriteriaQuery<Author>) cq1).select(user1);  
	        TypedQuery<Author> tq1 = entitymanager.createQuery(select1);  
	        java.util.List<Author> list1 = tq1.getResultList();  
	        
	        System.out.println("sayzı" + list1.size());
	        size = list1.size();
	    	
	    }
	    
	    
        if(size == 1) {
        	info = "Bu mail ile kayıtlı kullanıcı mevcut";
        	return 0;
        	
        }
        
	    
		
         if(userName.equals("") || pw_again.equals("")|| e_mail.equals("") || password.equals("") || position.equals("")) {
			   info = "boş bırakılan yerleri tekrar kontrol edin";
		   }
		else if(!password.equals(pw_again)) {
			   System.out.println("eşleşmiyor");
			   System.out.println(e_mail);
			   info= "passworld'ler birbirleriyle eşleşmiyor" + e_mail;
  }
		   else if(position.equals("User")) {
			  	   

			   EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
				EntityManager entitymanager = emfactory.createEntityManager( );
			    entitymanager.getTransaction().begin( );
	    
	    
	    User user = new User(); 
	    user.setName(userName);
	    user.setEmail(e_mail);
	    user.setPassword(password);
	    user.setPw_again(pw_again);
	    user.setPosition(position);
	    entitymanager.persist( user );
	    entitymanager.getTransaction( ).commit( );

	    entitymanager.close( );
	    emfactory.close( );
		   info = "Kaydolma işlemi başarıyla tamamlandı";

	    
	}
		   else {
			   EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
				EntityManager entitymanager = emfactory.createEntityManager( );
			       entitymanager.getTransaction( ).begin( );

			    Author user = new Author(); 
			    user.setName(userName);
			    user.setEmail(e_mail);
			    user.setPassword(password);
			    user.setPw_again(pw_again);
			    user.setPosition("Author");

			    entitymanager.persist( user );
			    entitymanager.getTransaction( ).commit( );

			    entitymanager.close( );
			    emfactory.close( );
				   info = "Kaydolma işlemi başarıyla tamamlandı";

			    
			}
				 userName = "";
				  e_mail = "";
				  password = "";
				  pw_again = "";
				System.out.println(position);
			   
			   
			   
			   
			   
			   return 1;
			   
		
	}

public void createEmployee() {
	// TODO Auto-generated method stub
	

}

}

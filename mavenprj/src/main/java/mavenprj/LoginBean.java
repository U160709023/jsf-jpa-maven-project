package mavenprj;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entities.Author;
import entities.Books;
import entities.User;

@ManagedBean(name= "loginbean")
@SessionScoped
public class LoginBean {
	
	
	String email;

	String password;
	String info;
	boolean Author;
	String name;
	 String size;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	
	public String controlling() {
		
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
		EntityManager entitymanager = emfactory.createEntityManager( );
	    entitymanager.getTransaction( ).begin( );
	    
	    CriteriaBuilder cb=entitymanager.getCriteriaBuilder();  
        AbstractQuery<User> cq1=cb.createQuery(User.class);  
        Root<User> user1=cq1.from(User.class);  

        cq1.where(cb.equal(user1.get("email"),email ));
		
        CriteriaQuery<User> select1 = ((CriteriaQuery<User>) cq1).select(user1);  
        TypedQuery<User> tq1 = entitymanager.createQuery(select1);  
        java.util.List<User> list1 = tq1.getResultList();  
        
        System.out.println("sayzı" + list1.size() 	);
        
        if(list1.size() > 0) {
        	if( list1.get(0).getPassword().equals(password)){
        	name = list1.get(0).getName();
        	size = "0";

        	}
        	
        	return "/sayfa.xhtml";
           
        }
        else {
        	
        	return controllingForAuthor();
        }
        
	}
	
public String controllingForAuthor() {
		
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
		EntityManager entitymanager = emfactory.createEntityManager( );
	    entitymanager.getTransaction( ).begin( );
	    
	    CriteriaBuilder cb=entitymanager.getCriteriaBuilder();  
        AbstractQuery<Author> cq1=cb.createQuery(Author.class);  
        Root<Author> user1=cq1.from(Author.class);  

        cq1.where(cb.equal(user1.get("email"),email ));
		
        CriteriaQuery<Author> select1 = ((CriteriaQuery<Author>) cq1).select(user1);  
        TypedQuery<Author> tq1 = entitymanager.createQuery(select1);  
        java.util.List<Author> list1 = tq1.getResultList();  
        
        System.out.println("sayzı" + list1.size()	);
        
        if(list1.size() > 0 ) {
        	if(list1.get(0).getPassword().equals(password)) {
        	name = list1.get(0).getName();
        	size = "1";
        	System.out.println("login sayz" + size); 
        	}
        	return "/sayfa.xhtml";
           
        }
        else {
        	
        	 return "/index.xhtml";
        }
        
	}

public boolean isAuthor() {
	return Author;
}

public void setAuthor(boolean author) {
	Author = author;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getSize() {
	return size;
}

public void setSize(String size) {
	this.size = size;
}
	
	
	

}

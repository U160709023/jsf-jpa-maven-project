package mavenprj;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.persistence.internal.jpa.parsing.Node;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.google.protobuf.compiler.PluginProtos.CodeGeneratorResponse.File;
import com.sun.faces.facelets.util.Path;

import entities.Books;
import entities.Library;
import entities.User;

@ManagedBean(name = "librarybean")
@SessionScoped
public class LibraryBean {

	List<Books> booksList;
	static Books selectedBook;
	  private String path;
	    private String contentType;

	public List<Library> ShowMeLibrary(String email) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Eclipselink_JPA");  
        EntityManager em=emf.createEntityManager();  
        em.getTransaction( ).begin( );
		
		
		
 
		////////////////////////////////////////////////////////////////

        CriteriaBuilder cb=em.getCriteriaBuilder();  
        AbstractQuery<Library> cq1=cb.createQuery(Library.class);  
        Root<Library> user1=cq1.from(Library.class);  

        cq1.where(cb.equal(user1.get("usermail"),email ));
		
        CriteriaQuery<Library> select1 = ((CriteriaQuery<Library>) cq1).select(user1);  
        TypedQuery<Library> tq1 = em.createQuery(select1);  
        java.util.List<Library> list1 = tq1.getResultList();  
      if(list1.size()>0) {
        	
        	return list1;}
      else return null;
        
       
        
	}
	public String openFile( String file ) {
		System.out.println("dosyamız" + file);
		return file;
		}
	
	
	
	
	public void downloadAction(String path) {
		 java.io.File file = new java.io.File(path);
		    HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();  

		    response.setHeader("Content-Disposition", "attachment;filename=file.txt");  
		    response.setContentLength((int) file.length());  
		    ServletOutputStream out = null;  
		    try {  
		        FileInputStream input = new FileInputStream(file);  
		        byte[] buffer = new byte[1024];  
		        out = response.getOutputStream();  
		        int i = 0;  
		        while ((i = input.read(buffer)) != -1) {  
		            out.write(buffer);  
		            out.flush();  
		        }  
		        FacesContext.getCurrentInstance().getResponseComplete();  
		    } catch (IOException err) {  
		        err.printStackTrace();  
		    } finally {  
		        try {  
		            if (out != null) {  
		                out.close();  
		            }  
		        } catch (IOException err) {  
		            err.printStackTrace();  
		        }  
		    }  
    }

    public StreamedContent getdFile() throws IOException {
    	System.out.println("indiriliyor");
        return new DefaultStreamedContent(new FileInputStream(path), contentType);
    }
	
	
	
	
	
	
	
	
	public void oku() {
		// TODO Auto-generated method stub

	}

	public List<Books> getBooksList() {
		return booksList;
	}

	public void setBooksList(List<Books> booksList) {
		this.booksList = booksList;
	}
	public static Books getSelectedBook() {
		return selectedBook;
	}
	public static void setSelectedBook(Books selectedBook) {
		LibraryBean.selectedBook = selectedBook;
	}
	
	
}

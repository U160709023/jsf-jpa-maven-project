package mavenprj;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class TabMenuManagedBean {
	
	String messageNum1 = "wqewq";
	String messageNum2 = "wqewq";

	private int index = 1;
	
	

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getMessageNum1() {
		return messageNum1;
	}

	public void setMessageNum1(String messageNum1) {
		this.messageNum1 = messageNum1;
	}

	public String getMessageNum2() {
		return messageNum2;
	}

	public void setMessageNum2(String messageNum2) {
		this.messageNum2 = messageNum2;
	}

	public String doSomeWork(){
		// Do some work
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Some Work Achieved"));
		// Change the index that TabMenu refers as activated tab
		index = 1;
		return "";
	}
}
package mavenprj;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PostLoad;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entities.Author;
import entities.Books;
import entities.Library;
import entities.User;

@ManagedBean(name="marketbean")
@RequestScoped
public class MarketBean {

	
	List<Books> booksList;
	static Books selectedBook;
		
	    public List<Books> init() {
	        booksList = new ArrayList<Books>();
	        int a = 1;
	        
	        
	        
	        
	        
	        EntityManagerFactory emf = Persistence.createEntityManagerFactory( "Eclipselink_JPA");  
	          EntityManager em = emf.createEntityManager();  
	          em.getTransaction().begin( );  
	          CriteriaBuilder cb=em.getCriteriaBuilder();  
	          CriteriaQuery<Books> cq=cb.createQuery(Books.class);  
	            
	         Root<Books> books=cq.from(Books.class);  
	           
	         cq.select(books);   
	           
	          CriteriaQuery<Books> select = cq.select(books);  
	          TypedQuery<Books> q = em.createQuery(select);  
	          List<Books> list = q.getResultList();  
	  
	          System.out.println("s_id");  
	               
	            
	          for(Books s:list)  
	          {  
	          System.out.println("authorlar" + s.getAuthor() + " " +a);  
	          a++;
	      
	        }  
	            
	em.getTransaction().commit();  
	          em.close();  
	          emf.close();   
	          return list;
	     
	}

		public List<Books> getBooksList() {
			return booksList;
		}

		public void setBooksList(List<Books> booksList) {
			this.booksList = booksList;
		}

		public Books getSelectedBook() {
			return selectedBook;
		}

		public void setSelectedBook(Books selectedBook) {
			this.selectedBook = selectedBook;
		}  
	    
	    public String act() {
			// TODO Auto-generated method stub
	    	return "acting";
		}
	    
	    public String photo() {
			// TODO Auto-generated method stub
	    		return "/PP.jpg";
		}
	    
	    
	    public User myUser(String email) {
	    	EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
			EntityManager entitymanager = emfactory.createEntityManager( );
		    entitymanager.getTransaction( ).begin( );
		    
		    CriteriaBuilder cb=entitymanager.getCriteriaBuilder();  
	        AbstractQuery<User> cq1=cb.createQuery(User.class);  
	        Root<User> user1=cq1.from(User.class);  

	        cq1.where(cb.equal(user1.get("email"),email ));
			
	        CriteriaQuery<User> select1 = ((CriteriaQuery<User>) cq1).select(user1);  
	        TypedQuery<User> tq1 = entitymanager.createQuery(select1);  
	        java.util.List<User> user = tq1.getResultList();  
	        
	        System.out.println("bulunan user = " + user.get(0).getEmail());
	        if(user.size() >0) {
	    	return user.get(0);
	    	}
	        
	        	return null;
	        
	    }
	    
	    public Long canDelete(Books book,String email) {
	    	
	      
	        	
	        	if(book.getAuthor().equals(email)) {
	        		return (long) 1;
	        	}
	        	else {return (long) 2;}
	        	
	        	
	        
	    	
	    }
	    
	    public void delete(Books book) {
	    	 EntityManagerFactory emf=Persistence.createEntityManagerFactory("Eclipselink_JPA");  
	    	    EntityManager em=emf.createEntityManager();  
	    	em.getTransaction().begin();  
	    	  System.out.println("tıkladığın kitap" + book.getBookName());
	    	    Books s=em.find(Books.class,book.getUid());  
	   	    Library l = em.find(Library.class, book.getUid() );
	    	em.remove(s);  
	    	em.remove(l);
	    	em.getTransaction().commit();  
	    	emf.close();  
	    	em.close();  
	    	
	    	
	    	
	    	EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
			EntityManager entitymanager = emfactory.createEntityManager( );
		    entitymanager.getTransaction( ).begin( );
		    
		    CriteriaBuilder cb=entitymanager.getCriteriaBuilder();  
	        AbstractQuery<Library> cq1=cb.createQuery(Library.class);  
	        Root<Library> user1=cq1.from(Library.class);  

	        cq1.where(cb.equal(user1.get("b_id"),book.getUid() ));
			
	        CriteriaQuery<Library> select1 = ((CriteriaQuery<Library>) cq1).select(user1);  
	        TypedQuery<Library> tq1 = entitymanager.createQuery(select1);  
	        java.util.List<Library> list1 = tq1.getResultList();  
	        
	        if(list1.size() > 0) {
	        	Library lq = entitymanager.find(Library.class, book.getUid() );
	        	entitymanager.remove(lq);
	        	
	        	
	        }
	    	
	    	
	        entitymanager.getTransaction().commit();
        	emfactory.close();
        	entitymanager.close();
	    	
	    	
	    }
	    
	    public void addtoLibrary(Books book,String email) {
			// TODO Auto-generated method stub
	    	System.out.println("tıkladın"+ book.getBookName() + "mailin" + email);

		       
	          
	    	
	    	EntityManagerFactory emf=Persistence.createEntityManagerFactory("Eclipselink_JPA");  
	        EntityManager em=emf.createEntityManager();  
	          
	        em.getTransaction().begin();  
	          
	        
	        
	        
	        
	        CriteriaBuilder cb=em.getCriteriaBuilder();  
	        AbstractQuery<User> cq1=cb.createQuery(User.class);  
	        Root<User> user1=cq1.from(User.class);  

	        cq1.where(cb.equal(user1.get("email"),email ));
			
	        CriteriaQuery<User> select1 = ((CriteriaQuery<User>) cq1).select(user1);  
	        TypedQuery<User> tq1 = em.createQuery(select1);  
	        java.util.List<User> list1 = tq1.getResultList();  
	        
	        
	        if(list1.size()>0) {
	        
		    Library lib = new Library(); 
		    lib.setB_id(book.getUid());
		    lib.setB_name(book.getBookName());
		    lib.setPath(book.getFileName());
		    lib.setUsermail(email);
		    em.persist( lib );
		    em.getTransaction( ).commit( );

		    em.close( );
		    em.close( );
	        
	        
	        }
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        em.getTransaction().commit();  
	        em.close();  
	        emf.close();
		}




		public String openFile( String file ) {
			System.out.println("dosyamız" + file);
			return file;
			}
		
		


	    
	    
}

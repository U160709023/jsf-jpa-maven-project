# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Sharing book by author and the users can get the books from market to download and read them.


### How do I get set up? ###
0) You should change the "property" in "persistence.xml" for database connection. It can be different from mime.
1) If your OS is Windows , you should change the variable "destination" in "UploadFileBean.java" in line 43. If your OS is linux. Make no changes.
2) Deploy the project to tomcat server and go to "http://localhost:8080/helloworld/". This page is registration form.
3) If you register as Author, you can release and delete books on "release book" page. But you can not add any book to your library because you are not writer.
4) If you register as User, you can add any book to your library from market page. But you can not make any changes on Market(Adding and deleting books). Because you are not author.
5) If you delete a book from market and a user added that book to his library, that book is deleted from market bot not from his library.
